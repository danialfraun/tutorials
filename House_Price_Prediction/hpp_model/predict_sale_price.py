import pandas as pd
import numpy as np
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split


class HPP():
    def __init__(self, MSSubClass, LotArea, YearBuilt, TotRmsAbvGrd, GarageCars, GrLivArea, OverallQual) -> None:
        self.MSSubClass = MSSubClass
        self.LotArea = LotArea
        self.YearBuilt = YearBuilt
        self.TotRmsAbvGrd = TotRmsAbvGrd
        self.GarageCars = GarageCars
        self.GrLivArea = GrLivArea
        self.OverallQual = OverallQual
        self.data_preparation()
        self.prediction()

    def data_preparation(self):
        '''
        This function reads the train.csv dataset. We further pick the most important and relevant features and further normalize
        them to a standard scale.
        '''

        # Read the data and store in a dataframe called training_set
        train_data_path = 'train.csv'
        training_set = pd.read_csv(train_data_path)

        # Create a list of the predictor variables
        # Refer the READE.MD to understand the feature selection process
        self.predictors = [ "MSSubClass", "LotArea", "YearBuilt", "TotRmsAbvGrd", "GarageCars", "GrLivArea", "OverallQual"]

        # Select the dataframe with the necessary input features
        self.X = training_set[self.predictors]

        # Select the target variable and call it y
        self.y = training_set.SalePrice

        #splitting the dataset
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.X, self.y, test_size=0.2, random_state=42)

        # Normalization of features in the dataset
        self.scaler_x = MinMaxScaler() 
        self.scaler_y = MinMaxScaler()

        # Scale the training input variables
        self.X_train_scaled = self.scaler_x.fit_transform(self.X_train)
        self.X_test_scaled = self.scaler_x.transform(self.X_test)

        # Scale the training target variables
        y_train_array = self.y_train.values.reshape(-1, 1)
        y_test_array = self.y_test.values.reshape(-1, 1)
        self.y_train_scaled = self.scaler_y.fit_transform(y_train_array)
        self.y_test_scaled = self.scaler_y.transform(y_test_array)
    
    def prediction(self):
        '''
        This function defines the model and predicts the required output for the inputs entered by the user in the web-UI.
        '''

        # Define the model
        tree_model = DecisionTreeRegressor(splitter='random', random_state=2)

        # Fit model 
        tree_model.fit(self.X_train_scaled, self.y_train_scaled)

        # Predict the SalePrice output with the inputs from the web-UI as entered by the user
        y_pred_UI = tree_model.predict(self.scaler_x.transform([[self.MSSubClass, self.LotArea, self.YearBuilt, self.TotRmsAbvGrd, 
                                          self.GarageCars, self.GrLivArea, self.OverallQual]]))
        self.true_ypred = tree_model.predict(self.X_test_scaled)
        # Rescale the predicted SalePrice in accordance to the standard scale
        y_pred_UI = self.scaler_y.inverse_transform(y_pred_UI.reshape(-1, 1))
        

        return y_pred_UI

    def calculate_metrics(self):
        # measure 
        # Calculate MSE
        
        mse = mean_squared_error(self.y_test_scaled, self.true_ypred)
        rmse = np.sqrt(mse)
     
        #Calculate R² score
        r_squared = r2_score(self.y_test_scaled, self.true_ypred)

        # Calculate adjusted R² score
        n = len(self.y_test_scaled)
        k = self.X.shape[1]
        adjusted_r_squared = 1 - ((1 - r_squared) * (n - 1) / (n - k - 1))
       

        return mse, rmse, r_squared, adjusted_r_squared



def predict_sale_price(MSSubClass, LotArea, YearBuilt, TotRmsAbvGrd, GarageCars, GrLivArea, OverallQual):
    hpp = HPP(MSSubClass, LotArea, YearBuilt, TotRmsAbvGrd, GarageCars, GrLivArea, OverallQual)
    SalePrice_prediction = hpp.prediction()
    mse, rmse, r_squared, adjusted_r_squared = hpp.calculate_metrics()

    return SalePrice_prediction, mse, rmse, r_squared, adjusted_r_squared

    

