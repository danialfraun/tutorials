import grpc
from concurrent import futures
import threading
import logging

# import the generated classes :
import model_pb2
import model_pb2_grpc
from app import app_run
import datetime

# import the function we made :
import predict_sale_price as psp
#from predict_sale_price import predict_sale_price, regressor_metrics

port = 8061
results = []
# create a class to define the server functions, derived from


class PredictServicer(model_pb2_grpc.PredictServicer):
    def __init__(self):
        self.has_metrics = True # Flag to indicate the presence of metrics in this node and print a message accordingly.
        if self.has_metrics:
            logging.info('MetricsAvailable')
        

    def predict_sale_price(self, request, context):
        # define the buffer of the response :
        response = model_pb2.Prediction()
        # get the value of the response by calling the desired function :
        response.salePrice, *_ = psp.predict_sale_price(request.MSSubClass, request.LotArea, request.YearBuilt, request.TotRmsAbvGrd, request.GarageCars, request.GrLivArea, request.OverallQual)
        result = [request.MSSubClass, request.LotArea, request.YearBuilt, request.TotRmsAbvGrd, request.GarageCars, request.GrLivArea, request.OverallQual, response.salePrice]
        with open("results.txt", mode="a+") as f:
            # for e0, e1, e2, e3, e4, e5 in result:
            f.write(str(round(request.MSSubClass, 2)) + "|" + str(round(request.LotArea, 2)) + "|" + str(round(request.YearBuilt, 2)) + "|" + str(round(request.TotRmsAbvGrd, 2)) + "|" + str(round(request.GarageCars, 2)) + "|" + str(round(request.GrLivArea, 2)) + "|" + str(round(request.OverallQual, 2)) + "|" + str(round(response.salePrice, 2)) + "\n")
            f.close()
        
        return response

    #The function will get the metrics calculated in predict_sale_price.py    
    def regressor_metrics(self, request, context):
        response = model_pb2.TrainingStatus()
        _, response.mse, response.rmse, response.r_squared, response.adjusted_r_squared = psp.predict_sale_price(request.MSSubClass, request.LotArea, request.YearBuilt, request.TotRmsAbvGrd, request.GarageCars, request.GrLivArea, request.OverallQual)
        response.status_text = 'success'

        # Call the gRPC routine immediately after the training process concludes and the metrics have been recorded
        self.get_metrics_metadata(response, None)

        return response


    def get_metrics_metadata(self, request, context):
        
        final_metrics_dict = {'metrics': {}}
        now = datetime.datetime.now()
        current_time = now.strftime("%Y-%m-%d %H:%M:%S")
        final_metrics_dict['metrics']['date_time'] = current_time

        for field, value in request.ListFields():
            field_name = field.name
            final_metrics_dict['metrics'][field_name] = value

        logging.info(final_metrics_dict)



def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    model_pb2_grpc.add_PredictServicer_to_server(PredictServicer(), server)
    server.add_insecure_port('[::]:{}'.format(port))
    server.start()
    threading.Thread(target=app_run()).start()
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    open('results.txt', 'w').close()
    serve()
