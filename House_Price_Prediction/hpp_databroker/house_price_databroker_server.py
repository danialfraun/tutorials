import grpc
from concurrent import futures
import threading

# import the generated classes :
import model_pb2
import model_pb2_grpc
from app import app_run, get_parameters
import logging
import os

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
port = 8061


# create a class to define the server functions, derived from
class DatabrokerServicer(model_pb2_grpc.DatabrokerServicer):

    def __init__(self,datasetFeatures_filepath):
        super().__init__()
        self.send_data=True
        self.datasetFeatures_filepath = datasetFeatures_filepath
        request = model_pb2.DatasetFeatues()
        self.get_dataset_metadata(request, None)

    def hppdatabroker(self, request, context):
        # this simple example without streaming sends only one data record, then the pipeline must be run again
        parameters = get_parameters()
        logger.debug("Connecting to databroker")
        response = model_pb2.Features(MSSubClass=float(parameters[0]), LotArea=float(parameters[1]),
                                      YearBuilt=float(parameters[2]),
                                      TotRmsAbvGrd=float(parameters[3]), GarageCars=float(parameters[4]), GrLivArea=float(parameters[5]), OverallQual=float(parameters[6])
                                      )
        logger.debug(response)
        if not self.send_data:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details("all data has been processed")

        self.send_data= not self.send_data
        return response

    '''Function to read the dataset attributes'''
    def get_dataset_metadata(self, request, context):       

        descriptor = model_pb2.DatasetFeatues.DESCRIPTOR
        final_features_dict = {'dataset_features': {}}

        if os.path.isfile(self.datasetFeatures_filepath):

            with open(self.datasetFeatures_filepath) as fp:
                features_dict = dict(line.strip().split(',', 1) for line in fp)

            for field in descriptor.fields:
                field_name = field.name
                value = features_dict.get(field_name)

                if value:
                    final_features_dict['dataset_features'][field_name] = value
                    setattr(request, field_name, value)

            if final_features_dict['dataset_features']:
                logging.info(final_features_dict)
            else:
                logging.info('Dataset features file available, but does not contain any entries')
        else:
            logging.info('Dataset features file not available')


def serve():
    #Logging is enabled
    logging.basicConfig(level=logging.INFO)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    datasetFeatures_filepath = 'dataset_features.txt' # Input dataset attributes file
    model_pb2_grpc.add_DatabrokerServicer_to_server(DatabrokerServicer(datasetFeatures_filepath), server)
    
    server.add_insecure_port('[::]:{}'.format(port))
    logger.debug("Start server")
    server.start()
    logger.debug("Start databroker UI")
    threading.Thread(target=app_run()).start()
    server.wait_for_termination()
    logger.debug("Threads ended")


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    serve()
