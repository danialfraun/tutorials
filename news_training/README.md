# Training pipeline example 
The training pipeline for the news_training example consists of 4 nodes, with each residing in its own subdirectory:

* News-Classifier - The core of the pipeline
* News-Trainer - Facilitates the training process
* Tensorboard - Provides diagnostics preview
* News-Databroker - Starting point for data feed

Note: Apart from demonstrating a training scenario, this example also shows the use of a shared folder for common file access for pipeline nodes.


## New-Classifier Module
The classifier module is the core of the entire pipeline. It is responsible for the following activities,
* Training process: Upon receiving the training parameters from the trainer node, the classifier node starts the training process. The AI model structure is defined here.
* Saving the trained models: Upon successful training, the models are saved in both the h5 and onnx format in the shared folder.  
An instance of the onnx model for the given ML model is as shown below,
* Classifying the results: The Reuters dataset newswires are labeled over 46 topics. A sample classification result (confusion matrix) for the test sequences is as shown below,  

Model layers from onnx file             |  Confusion Matrix for the test sequences
:-------------------------:|:-------------------------:
<img src=sample_images/reuters_model.onnx.png width="180" height="950"/>  |  <img src=sample_images/cm_reuters.png width="350" />


## New-Trainer Module
The trainer node facilitates the training process by specifying the classifier node with the required hyperparameters. The hyperparameters as seen in the protobuf definition file is shown below,

The hyperparameters can be controlled/changed by the user in the web-UI.

```
message TrainingConfig {
  string training_data_filename = 1; // .npz file
  string training_labels_filename = 2; // .npz file
  int32 epochs = 3;
  int32 batch_size = 4;
  double validation_ratio = 5;
  string model_filename = 6;
}
```

## Testing individual nodes - Creating Clients
By creating clients for each of the nodes here, we can understand the individual node functionalities and execute and test a gRPC server.

In the client file we will do the following:
* Open a gRPC channel
* Create a stub
* Create a request message
* Use the stub to call the service

Please refer the classifier_client.py and trainer_client.py for the classifier and trainer respectively.


trainer_client.py
```python
def run_trainer():
    logging.info("Calling NewsTrainer_Stub...")

    # Open a gRPC channel
    with grpc.insecure_channel('localhost:8061') as channel:
        # Create a trainer stub
        trainer_stub = news_trainer_pb2_grpc.NewsTrainerStub(channel) 

        # Create a request message with the necessary training parameters
        trainer_client_request = news_trainer_pb2.TrainingConfig(
                  training_data_filename = shared_folder+'/reuters_training_data.npz',
                  training_labels_filename = shared_folder+'/reuters_training_labels.npz',
                  epochs = 9,
                  batch_size = 512,
                  validation_ratio = 0.1,
                  model_filename = shared_folder+'/reuters')

        # Call the service using the created stub
        response = trainer_stub.startTraining(trainer_client_request) 
        
        return response
```

classifier_client.py
```python
def run_classifier():
    logging.info("Calling NewsClassifier_Stub...")

    # Open a gRPC channel
    with grpc.insecure_channel('localhost:8062') as channel: 
        # Create a classifier stub
        classifier_stub = news_classifier_pb2_grpc.NewsClassifierStub(channel)

        # Create a request message with the necessary training parameters
        request_train = news_classifier_pb2.TrainingConfig(
                  training_data_filename = shared_folder+'/reuters_training_data.npz',
                  training_labels_filename = shared_folder+'/reuters_training_labels.npz',
                  epochs = 9,
                  batch_size = 512,
                  validation_ratio = 0.1,
                  model_filename = shared_folder+'/reuters_model')

        # Call the service using the created stub
        response_train = classifier_stub.startTraining(request_train)

        # Create a request message with the newswire text
        request_classify = news_classifier_pb2.NewsText(text='This is a news classifier. \
        Please enter the newwires here. You can then find their category!')

        # Call the service using the created stub
        response_classify = classifier_stub.classify(request_classify)
        
        return response_train, response_classify
```

## Building and running Docker images
Build the trainer docker image  
The respective Dockerfile is present inside the trainer directory
```commandline
docker build -t trainer_image .
```

Run the docker image
```commandline
docker run -v /path/to/the/shared_folder:/path/to/the/shared_folder -e SHARED_FOLDER_PATH=/path/to/the/shared_folder -it --rm -p 8061:8061 trainer_image:latest
```

In another terminal, run the client script and observe the request's response.
```commandline
python trainer_client.py
```

Similarly for the classifier node,  
Build the classifier docker image  
The respective Dockerfile is present inside the classifier directory
```commandline
docker build -t classifier_image .
```

Run the docker image
```commandline
docker run -v /path/to/the/shared_folder:/path/to/the/shared_folder -e SHARED_FOLDER_PATH=/path/to/the/shared_folder -it --rm -p 8062:8061 classifier_image:latest
```

In another terminal, run the client script and observe the request's response.
```commandline
python classifier_client.py
```
