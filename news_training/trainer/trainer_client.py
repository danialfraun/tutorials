import logging
import grpc

import news_trainer_pb2, news_trainer_pb2_grpc

shared_folder = '/home/slakshmana/dataset'

def run_trainer():
    logging.info("Calling NewsTrainer_Stub...")

    with grpc.insecure_channel('localhost:8061') as channel:
        trainer_stub = news_trainer_pb2_grpc.NewsTrainerStub(channel)

        trainer_client_request = news_trainer_pb2.TrainingConfig(
                                                    training_data_filename = shared_folder+'/reuters_training_data.npz',
                                                    training_labels_filename = shared_folder+'/reuters_training_labels.npz',
                                                    epochs = 3,
                                                    batch_size = 512,
                                                    validation_ratio = 0.1,
                                                    model_filename = shared_folder+'/reuters')

        logging.info("-------------- Start Training with the requested parameters--------------")
        response = trainer_stub.startTraining(trainer_client_request)
        
        return response


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    response = run_trainer()
